defmodule Alchemist.Tasks do

  def list_tasks(type) do
    if Enum.member?(["todos", "habits", "dailys"], type) do
      case Alchemist.Config.get_config do
        {:ok, config} -> 
          %{"api_key" => api_key, "user_id" => user_id} = config
          HTTPotion.start
          header = [
              "x-api-user": user_id,
              "x-api-key": api_key,
              "Content-Type": "application/x-www-form-urlencoded"
          ]
          case HTTPotion.get("https://habitica.com/api/v4/tasks/user?type=#{type}", headers: header) do
            %HTTPotion.Response{status_code: 200, body: body} ->
              body = Poison.decode!(body)
              IO.puts "Here are your tasks:"

              for n <- 1..length(body["data"]) do
                test = Enum.at(body["data"], n - 1)
                IO.puts "#{n} #{test["text"]}"
              end
              task_id = IO.gets "Which task you want to finish? "
              task_id = String.trim(task_id, "\n")
              task = Enum.at(body["data"], String.to_integer(task_id) - 1)

              finish_task(task)
          end
      end
    else
      IO.puts "Please provide valid parameter. Accepted values are: todos, habits, dailys"
    end
  end

  defp finish_task(task) do
    case Alchemist.Config.get_config do
      {:ok, config} -> 
        %{"api_key" => api_key, "user_id" => user_id} = config
        HTTPotion.start
        header = [
            "x-api-user": user_id,
            "x-api-key": api_key,
            "Content-Length": 0
        ]

        case HTTPotion.post "https://habitica.com/api/v3/tasks/#{task["_id"]}/score/up", headers: header do
          %HTTPotion.Response{status_code: 200, body: _body} ->
            IO.puts "Task #{task["text"]} finished!"
        end
    end
  end

  def new_task(type) do
    if Enum.member?(["todo", "habit", "daily"], type) do
      case Alchemist.Config.get_config do
        {:ok, config} -> 
          %{"api_key" => api_key, "user_id" => user_id} = config
          HTTPotion.start
          header = [
              "x-api-user": user_id,
              "x-api-key": api_key,
              "Content-Type": "application/x-www-form-urlencoded"
          ]
      
          title = IO.gets "Title of the task: "
          title = String.trim(title, "\n")
      
          notes = IO.gets "Additional notes: "
          notes = String.trim(notes, "\n")
      
          difficulty = IO.gets """
          Set task difficulty:
            1. trivial
            2. easy
            3. medium
            4. hard
          """
          difficulty = String.to_integer(String.trim(difficulty, "\n"))
          diff_levels = [0.1, 1, 1.5, 2]
          options = URI.encode_query(%{
            text: title,
            notes: notes,
            type: type,
            priority: Enum.at(diff_levels, difficulty)
          })

          case HTTPotion.post "https://habitica.com/api/v3/tasks/user", [body: options, headers: header] do
            %HTTPotion.Response{status_code: 201, body: body} ->
              body = Poison.decode!(body)
              IO.puts "#{body["data"]["type"]} '#{body["data"]["text"]}' created!"
          end

        {:error, reason} -> IO.puts reason
      end
    else
      IO.puts "Please provide valid parameter. Accepted values are: todos, habits, dailys"
    end
  end
end
