defmodule Alchemist do

  # alias Alchemist.Todos
  use ExCLI.DSL, escript: true, mix_task: :al

  HTTPotion.start

  name "Habitica Alchemist"
  description "Habitica CLI client"
  long_description """
  Tak.
  """
  default_command :user

  command :user do
    description "Show user's stats"

    run _context do
      get_user()
    end
  end

  command :ls do
    description "Show user's tasks"
    long_description """
    Fetch all todos
    """

    argument :type

    run context do
      Alchemist.Tasks.list_tasks context.type
    end
  end

  command :create do
    description "Create new todo"
    long_description """
    New todo
    """
    
    argument :type
    run context do
      Alchemist.Tasks.new_task context.type
    end
  end

  def get_user do
    case Alchemist.Config.get_config do
      {:ok, config} -> 
        %{"api_key" => api_key, "user_id" => user_id} = config
        HTTPotion.start
        header = [
            "x-api-user": user_id,
            "x-api-key": api_key,
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        case HTTPotion.get("https://habitica.com/api/v4/user", headers: header) do
          %HTTPotion.Response{status_code: 200, body: body} ->
            %{"data" => %{"profile" => %{"name" => username}, "stats" => %{"hp" => hp, "mp" => mp, "exp" => _exp, "gp" => _gp, "lvl" => lvl, "class" => class,
            "maxHealth" => maxHP, "maxMP" => maxMP, "toNextLevel" => toNextLevel}}} = Poison.decode!(body)
            IO.puts "Hi, #{class} #{username}. HP: #{Kernel.trunc(hp)}/#{maxHP}, MP: #{mp}/#{maxMP}, level: #{lvl}. Exp to the next level: #{toNextLevel}" 
        end
      end
  end


end

ExCLI.run!(Alchemist)
