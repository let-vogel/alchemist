defmodule Alchemist.Config do

  def get_config do
    case File.read("./config.json") do
      {:ok, config} ->
        {:ok, Poison.decode!(config)}
      {:error, reason} ->
        {:error, reason}
    end  
  end

end